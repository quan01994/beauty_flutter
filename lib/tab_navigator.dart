import 'package:beauty_flutter/content_pager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabNavigator extends StatefulWidget {
  @override
  _TabNavigatorState createState() => _TabNavigatorState();
}

class _TabNavigatorState extends State<TabNavigator> {
  final _defaultColor = Colors.grey;
  final _activeColor = Colors.blue;
  int _currentIndex = 0;
  final _contentPageController = ContentPageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xffedeef0), Color(0xffe6e7e9)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: ContentPager(
          onPageChanged: (int index) {
            setState(() {
              _currentIndex = index;
            });
          },
          contentPageController: _contentPageController,
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          onTap: (index) {
            _contentPageController.jumpToPage(index);
            setState(() {
              _currentIndex = index;
            });
          },
          type: BottomNavigationBarType.fixed,
          items: [
            _bottomItems('本周', Icons.folder, 0),
            _bottomItems('分享', Icons.explore, 1),
            _bottomItems('免费', Icons.donut_small, 2),
            _bottomItems('长安', Icons.person, 3),
          ]),
    );
  }

  _bottomItems(String title, IconData iconData, int index) {
    return BottomNavigationBarItem(
        icon: Icon(
          iconData,
          color: _defaultColor,
        ),
        activeIcon: Icon(
          iconData,
          color: _activeColor,
        ),
        title: Text(
          title,
          style: TextStyle(
              color: _currentIndex != index ? _defaultColor : _activeColor),
        ));
  }
}
