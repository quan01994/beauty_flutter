import 'package:beauty_flutter/base_card.dart';
import 'package:flutter/material.dart';

class CardRecommend extends BaseCard {
  @override
  _CardRecommendState createState() => _CardRecommendState();
}

class _CardRecommendState extends BaseCardState {
  @override
  void initState() {
    // TODO: implement initState
    subTitleColor = Color(0xffb99444);
    super.initState();
  }

  @override
  bottomContent() {
    // TODO: implement bottomContent
    return Expanded(
      child: Container(
        //通过BoxConstraints尽可能撑满父容器
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(color: Colors.red),
        margin: EdgeInsets.only(top: 20),
        child: Image.network(
          'http://www.devio.org/io/flutter_beauty/card_1.jpg',
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  @override
  topTitle(String title) {
    // TODO: implement topTitle
    return super.topTitle('本周推荐');
  }

  @override
  Widget subTitle(String title) {
    // TODO: implement subTitle
    return super.subTitle('送你一天无限卡，全场书籍免费读');
  }
}
